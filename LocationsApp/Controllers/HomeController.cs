﻿using LocationsApp.Models;
using System.Web.Mvc;

namespace LocationsApp.Controllers
{
    public class HomeController : Controller
    {
        private IAppDbContext db;

        public HomeController()
        {
            db = new AppDbContext();
        }

        public HomeController(IAppDbContext dbContext)
        {
            db = dbContext;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var locationsViewModel = GetUpdatedLocationsViewModel(db);
            return View("Index", locationsViewModel);
        }

        [HttpPost]
        public ActionResult Create(Location location)
        {
            var address = location.Address;

            if (ModelState.IsValid) 
            {
                db.Locations.Add(location);
                db.SaveChanges();

                ViewBag.ResultMessage = "Location saved!";
            }
            else
            {
                ViewBag.ResultMessage = "Location not saved; both Name and Address are required, and the Address must be valid.";
            }

            var locationsViewModel = GetUpdatedLocationsViewModel(db);
            return PartialView("_Index", locationsViewModel);
        }

        [HttpGet]
        public ActionResult Details(int locationId)
        {
            var location = db.Locations.Find(locationId);
            return View(location);
        }

        #region private methods
        private LocationsViewModel GetUpdatedLocationsViewModel(IAppDbContext dbContext)
        {
            var locationsViewModel = new LocationsViewModel();

            foreach (var location in dbContext.Locations)
            {
                locationsViewModel.Locations.Add(location);
            }

            return locationsViewModel;
        }
        #endregion

    }
}