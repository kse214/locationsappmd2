﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace LocationsApp.Models
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext() : base("DefaultConnection")
        {
        }

        public IDbSet<Location> Locations { get; set; }
    }

    public interface IAppDbContext
    {
        int SaveChanges();

        IDbSet<Location> Locations { get; set; }
    }
}