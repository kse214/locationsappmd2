﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace LocationsApp.Models
{
    public class LocationsViewModel
    {
        public LocationsViewModel()
        {
            Locations = new Collection<Location>();
        }

        public ICollection<Location> Locations { get; set; }
    }
}